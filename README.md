# disk_io

[![build status](https://gitlab.com/zetok/disk_io/badges/master/pipeline.svg)](https://gitlab.com/zetok/disk_io/commits/master)

Tool that prints disks IO (read/written bytes) in 1s intervals.

Read | Written:

![in terminal](img/in_terminal.png)

Can be used e.g. with [i3blocks] ![with i3blocks](img/with_i3blocks.png),
with the following config:

```
# Disks I/O
[disk IO]
command=$PATH_TO_BINARY/disk_io
label=i/o:
interval=persist
min_width=115
color=#5ac1bd
```

## Binary

You can download binary from releases.

## Building
Fairly simple. You'll need [Rust] and [Cargo].

Build release version with:

```sh
cargo build --release
```

## License

Licensed under GPLv3+. For details, see [COPYING](/COPYING).


[Cargo]: https://doc.rust-lang.org/cargo/
[Rust]: https://www.rust-lang.org/
[i3blocks]: https://github.com/vivien/i3blocks
