/*
    Copyright © 2016, 2018 Zetok Zalbavar <zexavexxe@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::num::ParseIntError;
use std::ops::{AddAssign, Sub};
use std::str::FromStr;
use std::thread;
use std::time::Duration;

#[macro_use]
extern crate lazy_static;

extern crate regex;
use regex::Regex;

// read data from `/proc/diskstats`, "grep" lines by `sd[a-z]+`, add read/writes
// print diff between last counters and current ones in KB/s / MB/s from
// sectors, and after that sleep for 1s before repeating
//
// based https://www.kernel.org/doc/Documentation/iostats.txt


/// Number of bytes in a sector.
const BYTES_IN_SECTOR: u64 = 512;

/// Number of bytes in a MiB.
const BYTES_IN_MB: f64 = 1024.0 * 1024.0;

/// Number of bytes in a KiB.
const BYTES_IN_KB: f64 = 1024.0;

/// Path to file containing stats.
const DISKSTATS_PATH: &str = "/proc/diskstats";


/// Read/written sectors.
#[derive(Debug, Clone, Copy, Default, Eq, PartialEq)]
struct Sectors {
    r: u64,
    w: u64,
}

impl Sectors {
    fn from_txt(stats: &str) -> Result<Self, ParseIntError> {
        /*
        Get a number of read/written sectors from string that contains a single
        line read from `DISKSTATS_PATH` file.
         */
        let get_rw_sectors = |line: &str| {
            let strs: Vec<&str> = line.split_whitespace().collect();

            Ok(Sectors {
                r: u64::from_str(strs[5])?,
                w: u64::from_str(strs[9])?,
            })
        };

        lazy_static! {
            static ref RE: Regex = Regex::new(r"\ssd[[:alpha:]]+\s").unwrap();
        }

        // get only disks
        let disks: Vec<&str> = stats.lines()
            .filter(|l| RE.is_match(l)).collect();

        let mut sectors = Self::default();
        for disk in disks {
            sectors += get_rw_sectors(disk)?;
        }

        Ok(sectors)
    }

}

impl AddAssign for Sectors {
    fn add_assign(&mut self, other: Self) {
        *self = Sectors {
            r: self.r + other.r,
            w: self.w + other.w,
        };
    }
}

impl Sub for Sectors {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Sectors {
            r: self.r - other.r,
            w: self.w - other.w,
        }
    }
}

impl fmt::Display for Sectors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // convert to bytes before pretty-formatting
        let fmt_as_bytes = |s| pformat_num((s * BYTES_IN_SECTOR) as f64);
        write!(f, "{} {}", fmt_as_bytes(self.r), fmt_as_bytes(self.w))
    }
}

/// Pretty-format number of bytes.
fn pformat_num(num: f64) -> String {
    if num > BYTES_IN_MB {
        format!("{:.1}M", num / BYTES_IN_MB)
    } else { // KBs
        format!("{}k", num / BYTES_IN_KB)
    }
}

/// Get content of file with stats.
fn get_txt_stats() -> Result<String, std::io::Error> {
    let mut f = File::open(DISKSTATS_PATH)?;
    let mut read = String::new();
    f.read_to_string(&mut read)?;
    Ok(read)
}





fn main() {
    let mut last = Sectors::default();

    loop {
        thread::sleep(Duration::new(1, 0));

        let stats = get_txt_stats().expect("Failed to get stats");
        let new = Sectors::from_txt(&stats).expect("Failed to parse stats");

        // how much was read
        println!("{}", new - last);

        last = new;
    }
}


#[cfg(test)]
mod test {
    use ::*;
    // TODO: use quickcheck?

    #[test]
    fn pformat_num_t() {
        assert_eq!("0.5k", &pformat_num(512.0));
        assert_eq!("1k", &pformat_num(1024.0));
        assert_eq!("4k", &pformat_num(4096.0));
        assert_eq!("8k", &pformat_num(8192.0));
        // TODO: test cases for MBs
    }
}
